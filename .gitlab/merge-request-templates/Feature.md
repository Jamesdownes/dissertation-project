Completed user story: #Number UserStory

**Checklist**:

-   [ ] My code only uses ES6 syntax (eg. arrow functions)

-   [ ] I have merged the develop branch into my feature branch

-   [ ] I have manually run and tested my feature

-   [ ] I have added an appropriate number and scope of client side tests

-   [ ] I have added an appropriate number and scope of server side tests

-   [ ] I have added class comments to newly created components

-   [ ] I have added comments to my code as appropriate

-   [ ] I have set all of my tasks for the user story to 'ready for test' in the Taiga board
